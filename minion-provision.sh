#!/bin/sh

echo "setting /etc/resolv.conf"
cat > /etc/resolv.conf <<EOF
# Created by $0
# which was triggered by vagrant provision.
options timeout:2 attempts:3
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

echo "setting /etc/hosts"
cat > /etc/hosts <<EOF
# Created by $0
# which was triggered by vagrant provision.
127.0.0.1   localhost
192.168.56.10    salt
EOF


# Install latest salt-minion from our repository.
# We're suppressing output
echo "Installing salt-minion"
sudo zypper -n install salt-minion > /dev/null 2>&1 && echo "SUCCESS: Installed salt-minion"\
        || echo "FAIL: Could not install salt-minion";

mkdir -p /vagrant/minion.d
cp /vagrant/minion.d/* /etc/salt/minion.d/

echo "setting minion_id"
test -d /etc/salt && sudo hostname > /etc/salt/minion_id

if rpm -q salt-minion ; then
        echo "starting salt-minion service"
        sudo systemctl start salt-minion && sudo systemctl enable salt-minion
fi
