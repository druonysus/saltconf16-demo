saltconf16-demo
===============
**"How to create a great Salt Formula using Brine, a new open-source tool."**

Agenda
---
+ [Who is this "Drew" guy anyways?](https://www.google.com/#q=drew+opensuse)
+ [What does OpenX do?](http://www.openx.com)
+ What were the driving factors that make us create brine
+ [Look at a messy `init.sls` (es?)](https://gitlab.com/druonysus/saltconf16-demo/raw/master/master/salt/element/database/elasticsearch/init.sls)
+ [RabbitMQ Demo](http://192.168.56.11:15672/)
  + `element/queue/rabbitmq/`
  + `role/queue/mq-service/`  
+ [Nginx Demo](http://192.168.34.72//hello_saltconf16.html)
  + `element/network/nginx/`
  + `role/custom/saltconf-web/`
+ Major benafits in using Brine
+ The vision for brine
+ [How to contribute to Brine](http://github.com/openx/brine)
+ Q&A

