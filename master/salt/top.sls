base:
  # prod machines without salt roles
  '* and G@environment:prod':
    - match: compound
    - role.CORE
      
  {% for self in grains.roles %}
  'G@roles:{{ self }} and G@environment:prod':
    - match: compound
    - role.{{ self }}
  {% endfor %}

preprod:
  '* and not G@environment:prod':
    - match: compound
    - role.CORE

  {% for self in grains.roles %}
  'G@roles:{{ self }} and not G@environment:prod':
    - match: compound
    - role.{{ self }}
  {% endfor %}

