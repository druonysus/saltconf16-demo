CORE
====
*`CORE` role! This is an implicit role. Anything in **this role will get applied to every machine.***

Because this role will likely grow quite a bit over time, this role should mostly consists of included elements to compartmentalize and isolate configureation.

created with a little help from [Brine](https://github.com/openx/brine)
