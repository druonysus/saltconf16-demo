#
# CORE
#
#   `CORE` role! This is an implicit role. Anything in **this role will get applied to every machine.**
#


##
##  INCLUDES
##    http://docs.saltstack.com/en/latest/ref/states/include.html

include:
  - element.admin.system.motd
  - element.admin.system.bash
  - element.admin.system.tmux
  - element.admin.system.tcpdump
  - element.admin.system.net-tools

