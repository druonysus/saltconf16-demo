custom.saltconf-web
====
*Just a simple role that will display a static html file with some jinja rendered info*

This was created during a live demo at SaltConf16

created with a little help from [Brine](https://github.com/openx/brine)
