#
# custom.saltconf-web
#
#   Just a simple role that will display a static html file with some jinja rendered info
#


##
##  INCLUDES
##    http://docs.saltstack.com/en/latest/ref/states/include.html

include:
  - element.network.nginx


##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

custom.saltconf-web_/srv/www/htdocs/hello_saltconf16.html_file:
  file.managed:
    - name: /srv/www/htdocs/hello_saltconf16.html
    - source: salt://{{ slspath }}/files/srv/www/htdocs/hello_saltconf16.html.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root
