#
# admin.salt.master
#
#   This role will configure our Salt master.
#


##
##  INCLUDES
##    http://docs.saltstack.com/en/latest/ref/states/include.html

include:
  - element.admin.salt.minion


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

admin.salt.master_salt-master_pkg:
  pkg.latest:
    - name: salt-master

##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

admin.salt.master_/etc/salt/master_file:
  file.managed:
    - name: /etc/salt/master
    - source: salt://{{ slspath }}/files/etc/salt/master.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

admin.salt.master_/etc/salt/master.d/custom.conf_file:
  file.managed:
    - name: /etc/salt/master.d/custom.conf
    - source: salt://{{ slspath }}/files/etc/salt/master.d/custom.conf.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

##
##  SERVICES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.service.html

admin.salt.master_salt-master_svc:
  service.running:
    - name: salt-master
    - enable: True
