#
# queue.mq-service
#
#   This is the MQ Service. It currently is built on RabbitMQ.
#


##
##  INCLUDES
##    http://docs.saltstack.com/en/latest/ref/states/include.html

include:
  - element.queue.rabbitmq


##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

queue.mq-service_/var/lib/rabbitmq/.erlang.cookie_file:
  file.managed:
    - name: /var/lib/rabbitmq/.erlang.cookie
    - source: salt://{{ slspath }}/files/var/lib/rabbitmq/.erlang.cookie.jinja
    - template: jinja
    - makedirs: True
    - mode: '400'
    - user: rabbitmq
    - group: rabbitmq
