#
# queue.rabbitmq
#
#   A simple element that configs a rabbitmq server with management interface enabled
#


{% from "element/queue/rabbitmq/maps/versions.map.jinja" import versions with context %}


##
##  INCLUDES
##    http://docs.saltstack.com/en/latest/ref/states/include.html

include:
  - element.language.erlang


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

queue.rabbitmq_rabbitmq-server_pkg:
  pkg.installed:
    - name: rabbitmq-server
    - version: {{ versions['rabbitmq-server'] }}
    - refresh: True

queue.rabbitmq_rabbitmq-server-plugins_pkg:
  pkg.installed:
    - name: rabbitmq-server-plugins
    - version: {{ versions['rabbitmq-server-plugins'] }}
    - refresh: True

##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

queue.rabbitmq_/etc/rabbitmq/rabbitmq.config_file:
  file.managed:
    - name: /etc/rabbitmq/rabbitmq.config
    - source: salt://{{ slspath }}/files/etc/rabbitmq/rabbitmq.config.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

##
##  COMMANDS
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.cmd.html

run_queue.rabbitmq_/usr/sbin/rabbitmq-plugins_cmd:
  cmd.run:
    - name: /usr/sbin/rabbitmq-plugins enable rabbitmq_management

##
##  SERVICES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.service.html

queue.rabbitmq_rabbitmq-server_svc:
  service.running:
    - name: rabbitmq-server
    - enable: True

