queue.rabbitmq
====
*A simple element that configs a rabbitmq server with management interface enabled*

WARNING: This element doesn't provide a `.erlang.cookie` file... it is expected that you will manage that file in your role. You will want to have salt place the file in `/usr/lib/rabbitmq/.erlang.cookie`

created with a little help from [Brine](https://github.com/openx/brine)
