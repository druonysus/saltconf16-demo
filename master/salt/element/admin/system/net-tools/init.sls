#
# admin.system.net-tools
#
#   This is the package that provides the new depricated ifconfig command... we still want to use it, so this element will install it.
#


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

admin.system.net-tools_net-tools_pkg:
  pkg.installed:
    - name: net-tools
