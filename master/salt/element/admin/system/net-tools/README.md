admin.system.net-tools
            ====
            *This is the package that provides the new depricated ifconfig command... we still want to use it, so this element will install it.*

            this element is intended to be included in `core` role so it gets installed everywhere

	    created with a little help from [Brine](https://github.com/openx/brine)
