#
# admin.system.motd
#
#   Manages the `/etc/motd` file
#


##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

admin.system.motd_/etc/motd_file:
  file.managed:
    - name: /etc/motd
    - source: salt://{{ slspath }}/files/etc/motd.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root
