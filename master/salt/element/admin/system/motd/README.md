admin.system.motd
            ====
            *Manages the `/etc/motd` file*

            This element is intended to be included in `core` role, applied to all machines implicityly. There should be to reason for include it explicity in your role.

	    created with a little help from [Brine](https://github.com/openx/brine)
