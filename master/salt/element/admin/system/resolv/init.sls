#
# admin.system.resolv
#
#   manages the /etc/resolv.conf
#


##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

admin.system.resolv_/etc/resolv.conf_file:
  file.managed:
    - name: /etc/resolv.conf
    - source: salt://{{ slspath }}/files/etc/resolv.conf.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root
