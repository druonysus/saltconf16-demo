#
# admin.system.bash
#
#   install `bash`, make sure the `/etc/bash_completion.d` and manage files in
#


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

admin.system.bash_bash_pkg:
  pkg.installed:
    - name: bash

##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

admin.system.bash_/etc/bash_completion.d_dir:
  file.directory:
    - name: /etc/bash_completion.d
    - makedirs: True
    - mode: '0755'
    - user: root
    - group: root

admin.system.bash_/etc/bash.bashrc.local_file:
  file.managed:
    - name: /etc/bash.bashrc.local
    - source: salt://{{ slspath }}/files/etc/bash.bashrc.local.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root
