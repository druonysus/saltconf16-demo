#
# admin.system.tcpdump
#
#   install tcpdump package
#


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

admin.system.tcpdump_tcpdump_pkg:
  pkg.installed:
    - name: tcpdump
