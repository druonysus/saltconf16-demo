#
# admin.system.sudo
#
#   manages sudo
#


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

admin.system.sudo_sudo_pkg:
  pkg.installed:
    - name: sudo

##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

admin.system.sudo_/etc/sudoers.d_dir:
  file.directory:
    - name: /etc/sudoers.d
    - makedirs: True
    - mode: '0755'
    - user: root
    - group: root

admin.system.sudo_/etc/sudoers_file:
  file.managed:
    - name: /etc/sudoers
    - source: salt://{{ slspath }}/files/etc/sudoers.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

admin.system.sudo_/etc/sudoers.d/salt_file:
  file.managed:
    - name: /etc/sudoers.d/salt
    - source: salt://{{ slspath }}/files/etc/sudoers.d/salt.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

admin.system.sudo_/etc/sudoers.d/admins_file:
  file.managed:
    - name: /etc/sudoers.d/admins
    - source: salt://{{ slspath }}/files/etc/sudoers.d/admins.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

admin.system.sudo_/etc/sudoers.d/devs_file:
  file.managed:
    - name: /etc/sudoers.d/devs
    - source: salt://{{ slspath }}/files/etc/sudoers.d/devs.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

admin.system.sudo_/etc/sudoers.d/qa_file:
  file.managed:
    - name: /etc/sudoers.d/qa
    - source: salt://{{ slspath }}/files/etc/sudoers.d/qa.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root
