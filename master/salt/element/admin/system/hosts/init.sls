#
# admin.system.hosts
#
#   manages `/etc/hosts`
#


##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

admin.system.hosts_/etc/hosts_file:
  file.managed:
    - name: /etc/hosts
    - source: salt://{{ slspath }}/files/etc/hosts.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root
