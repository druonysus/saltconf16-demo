#
# admin.system.tmux
#
#   This element will just install tmux as it is a sysadmin's friend
#


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

admin.system.tmux_tmux_pkg:
  pkg.installed:
    - name: tmux
