admin.system.tmux
            ====
            *This element will just install tmux as it is a sysadmin's friend*

            This element is intended to be included into `core` role so it gets installed everywhere.

	    created with a little help from [Brine](https://github.com/openx/brine)
