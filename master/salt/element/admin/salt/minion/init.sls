#
# admin.salt.minion
#
#   manages the salt-minion
#


{% from "element/admin/salt/minion/maps/versions.map.jinja" import versions with context %}


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

admin.salt.minion_salt-minion_pkg:
  pkg.installed:
    - name: salt-minion
    - version: {{ versions['salt-minion'] }}
    - refresh: True

admin.salt.minion_python-pyzmq_pkg:
  pkg.installed:
    - name: python-pyzmq
    - version: {{ versions['python-pyzmq'] }}
    - refresh: True

##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

admin.salt.minion_/etc/salt/minion_file:
  file.managed:
    - name: /etc/salt/minion
    - source: salt://{{ slspath }}/files/etc/salt/minion.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

admin.salt.minion_/etc/salt/minion.d/custom.conf_file:
  file.managed:
    - name: /etc/salt/minion.d/custom.conf
    - source: salt://{{ slspath }}/files/etc/salt/minion.d/custom.conf.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

admin.salt.minion_/etc/salt/minion.d/beacons.conf_file:
  file.managed:
    - name: /etc/salt/minion.d/beacons.conf
    - source: salt://{{ slspath }}/files/etc/salt/minion.d/beacons.conf.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

##
##  SERVICES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.service.html

admin.salt.minion_salt-minion_svc:
  service.running:
    - name: salt-minion
    - enable: True
