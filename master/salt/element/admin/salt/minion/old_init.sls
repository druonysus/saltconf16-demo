#
#   Set up Salt Minion
#

## PACKAGES
# Install Salt Minion rpm
{% from "element/admin/salt/minion/map.jinja" import saltver with context %}
salt-minion-pkg:
  pkg.installed:
    - name: salt-minion
    - version:  {{ saltver }}
    - allow_updates: True

#upgrade the python-zmq version that's in int-salt-local repo
{% from "element/admin/salt/minion/map.jinja" import pythonzmqname with context %}
{% from "element/admin/salt/minion/map.jinja" import pythonzmqver with context %}
python-zmq-pkg:
  pkg.installed:
    - name: {{ pythonzmqname }}
    - version: {{ pythonzmqver }}
    - allow_updates: True

# Lets get our Salt Minion config onto our minions
salt-minion-config:
  file.managed:
    - name: /etc/salt/minion
    - mode: 644
    - replace: False

## FILES
openx-minion-config:
  file.managed:
    - name: /etc/salt/minion.d/openx.conf
    {% if grains['virtual'] == 'VirtualBox' %}
    - source: salt://{{ slspath }}/files/etc/salt/minion.d/openx.conf.masterless.template
    {% else %}
    - source: salt://{{ slspath }}/files/etc/salt/minion.d/openx.conf.template
    {% endif %}
    - makedirs: True
    - mode: 644
    - template: jinja

# Cron job script
salt-minion-cron-script:
  file.managed:
    - name: /usr/local/bin/salt-minion-fix.cron
    - source: salt://{{ slspath }}/files/usr/local/bin/salt-minion-fix.cron
    - user: root
    - mode: 755

admin.salt.minion_/usr/local/bin/salt-getgrains:
  file.managed:
    - name: /usr/local/bin/salt-getgrains
    - source: salt://{{ slspath }}/files/usr/local/bin/salt-getgrains
    - user: root
    - group: root
    - mode: '0755'

## CRON
# If for some reason salt crashed or was turned off
salt-minion-cron-job:
  cron.present:
    - name: /usr/local/bin/salt-minion-fix.cron
    - identifier: SALT_MINION_FIX
    - user: root
    - hour: 15
    - minute: 13


## SERVICES
# enable salt-minion through init
salt-minion-service:
  service.running:
    - name: salt-minion
    - enable: True
    - require:
      - pkg: salt-minion
    - watch:
      - file: /etc/salt/minion.d/openx.conf

# Only send logs from salt-minion to Sumo Logic on a few hosts
# Will base the sampling on the server_id which is a hash(fqdn)
{% set ratio = 64 -%}  # 0 is off, 1 is all hosts, and an integer > 1 will pick a few
{% set server_id = salt['grains.get']('server_id')|int(0) -%}
{%- if ratio != 0 and server_id % ratio == 0 -%} # Avoid divide by zero
salt-minion-syslog-config:
  file.managed:
    - name: /etc/rsyslog.d/30-salt-minion.conf
    - source: salt://{{ slspath }}/files/etc/rsyslog.d/30-salt-minion.conf.jinja2
    - makedirs: True
    - mode: 644
    - template: jinja

# Have rsyslog restart
## INCLUDES & EXCLUDES
include:
  - element.network.rsyslog

extend:
  rsyslog-daemon:
    service.running:
      - watch:
        - file: /etc/rsyslog.d/30-salt-minion.conf

{% else %} # Else make sure not logging, TODO need to reload rsyslogd once

salt-minion-syslog-config:
  file.absent:
    - name: /etc/rsyslog.d/30-salt-minion.conf

{%- endif -%}


