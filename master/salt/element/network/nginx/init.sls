#
# network.nginx
#
#   will set up a defualt nginx set up
#


{% from "element/network/nginx/maps/versions.map.jinja" import versions with context %}


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

network.nginx_nginx_pkg:
  pkg.installed:
    - name: nginx
    - version: {{ versions['nginx'] }}
    - refresh: True

##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

network.nginx_/srv/www/htdocs_dir:
  file.directory:
    - name: /srv/www/htdocs
    - makedirs: True
    - mode: '0755'
    - user: root
    - group: root

network.nginx_/etc/nginx/nginx.conf_file:
  file.managed:
    - name: /etc/nginx/nginx.conf
    - source: salt://{{ slspath }}/files/etc/nginx/nginx.conf.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

network.nginx_/etc/nginx/nginx.conf.default_file:
  file.managed:
    - name: /etc/nginx/nginx.conf.default
    - source: salt://{{ slspath }}/files/etc/nginx/nginx.conf.default.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

##
##  SERVICES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.service.html

network.nginx_nginx_svc:
  service.running:
    - name: nginx
    - enable: True
