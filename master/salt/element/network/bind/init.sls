#
# network.bind
#
#   This element controlls bind/named infrastructure.
#   it will cache recursive requests, and be authoritative for the `vagrant.saltconf16.demo` domain
#


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

network.bind_bind_pkg:
  pkg.installed:
    - name: bind

network.bind_bind-chrootenv_pkg:
  pkg.installed:
    - name: bind-chrootenv

network.bind_bind-utils_pkg:
  pkg.installed:
    - name: bind-utils

network.bind_bind-libs_pkg:
  pkg.installed:
    - name: bind-libs

##
##  FILES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html

network.bind_/var/lib/named_dir:
  file.directory:
    - name: /var/lib/named
    - makedirs: True
    - mode: '0755'
    - user: root
    - group: root

network.bind_/etc/named.conf_file:
  file.managed:
    - name: /etc/named.conf
    - source: salt://{{ slspath }}/files/etc/named.conf.jinja
    - template: jinja
    - makedirs: True
    - mode: '0644'
    - user: root
    - group: root

network.bind_/var/lib/named/vagrant.saltconf16.demo.zone_file:
  file.managed:
    - name: /var/lib/named/vagrant.saltconf16.demo.zone
    - source: salt://{{ slspath }}/files/var/lib/named/vagrant.saltconf16.demo.zone.jinja
    - template: jinja
    - makedirs: True
    - mode: '640'
    - user: root
    - group: root

network.bind_/var/lib/named/vagrant.saltconf16.demo.zone.rr_file:
  file.managed:
    - name: /var/lib/named/vagrant.saltconf16.demo.zone.rr
    - source: salt://{{ slspath }}/files/var/lib/named/vagrant.saltconf16.demo.zone.rr.jinja
    - template: jinja
    - makedirs: True
    - mode: '640'
    - user: root
    - group: root

##
##  SERVICES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.service.html

network.bind_named_svc:
  service.running:
    - name: named
    - enable: True
