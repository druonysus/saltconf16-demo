es-service-running:
  service.running:
    - name: elasticsearch
    - enable: true
    - require:
      - pkg: es-package
      - pkg: jdk-package
    - watch:
      - file: es-main-config

jdk-package:
  pkg.installed:
    - name: jdk

es-package:
  pkg.installed:
    - name: elasticsearch
    - version: 2.2.0-1

es-main-config:
  file.managed:
    - name: /etc/elasticsearch/elasticsearch.yml
    - source: salt://{{ slspath }}/files/etc/elasticsearch/elasticsearch.yml.template
    - template: jinja
    - makedirs: true
    - mode: '0644'
    - user: root
    - group: elasticsearch

es-main-config-original:
  file.managed:
    - name: /etc/elasticsearch/elasticsearch.yml.original
    - source: salt://{{ slspath }}/files/etc/elasticsearch/elasticsearch.yml.original
    - makedirs: true
