#
# language.erlang
#
#   This will install erlang
#


{% from "element/language/erlang/maps/versions.map.jinja" import versions with context %}


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

language.erlang_erlang_pkg:
  pkg.installed:
    - name: erlang
    - version: {{ versions['erlang'] }}
    - refresh: True
