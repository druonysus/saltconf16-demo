#
# language.openjdk
#
#   install openJDK headless
#


{% from "element/language/openjdk/maps/versions.map.jinja" import versions with context %}


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

language.openjdk_java-1_8_0-openjdk-headless_pkg:
  pkg.installed:
    - name: java-1_8_0-openjdk-headless
    - version: {{ versions['java-1_8_0-openjdk-headless'] }}
    - refresh: True
