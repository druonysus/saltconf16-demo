language.java.azul-zulu
====
*this element will install Azul's Java implementation based on openJDK (Zulu)*

**WARNING: This element was originally created using Brine, but has since been extended. Please do not run brine again or changes will be lost.

created with a little help from [Brine](https://github.com/openx/brine)
