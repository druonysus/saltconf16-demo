#
# language.java.azul-zulu
#
#   this element will install Azul's Java implementation based on openJDK (Zulu)
#


{% from "element/language/java/azul-zulu/maps/versions.map.jinja" import versions with context %}


##
##  REPOS
##    https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkgrepo.html

language.java.azul-zulu_azul_repo:
  pkgrepo.managed:
    - humanname: Azul Zulu (Java)
    - name: azul-zulu
    - baseurl: http://repos.azulsystems.com/sles/11


##
##  PACKAGES
##    http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

language.java.azul-zulu_zulu_pkg:
  pkg.installed:
    - name: {{ versions['zulu'] }}
    - refresh: True
