#!/bin/sh

echo "setting /etc/resolv.conf"
cat > /etc/resolv.conf <<EOF
# Created by $0
# which was triggered by vagrant provision.
options timeout:2 attempts:3
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

echo "setting /etc/hosts"
cat > /etc/hosts <<EOF
# Created by $0
# which was triggered by vagrant provision.
127.0.0.1   localhost
127.0.0.2   salt
EOF

# Install latest salt-minion from our repository.
# We're suppressing output
echo "Installing salt-minion and salt-master packages"
sudo zypper -n install salt salt-master salt-minion > /dev/null 2>&1 && echo "SUCCESS: Installed salt-minion and salt-master rpms"\
        || echo "FAIL: Could not install salt-minion";

mkdir -p /vagrant/minion.d /vagrant/master.d
touch /vagrant/minion.d/placeholder /vagrant/master.d/placeholder
cp /vagrant/minion.d/* /etc/salt/minion.d/
cp /vagrant/master.d/* /etc/salt/master.d/

echo "setting minion_id to `hostname`"
test -d /etc/salt && sudo hostname > /etc/salt/minion_id

echo "starting salt-master"
if rpm -q salt-master ; then
        sudo systemctl start salt-master && sudo systemctl enable salt-master
fi

echo "starting salt-minion"
if rpm -q salt-minion ; then
        sudo systemctl restart salt-minion && sudo systemctl enable salt-minion
fi

sleep 20s

echo "try to accept all pending minion keys"
sudo salt-key -y -A
